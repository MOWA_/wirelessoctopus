# Wireless Octopus
A simple way to wirelessly control an ESP32.

## Features

- Remotely control and monitor ESP32 peripherals (UART, GPIOs, ...) via a simple command set.
- Store activity traffic into log files which can be fetched through a file server

## Use cases

- Wireless HIL testing board 
- Simple logger (UART trace for example)
- ...

## Documentation

Documentation can be found in the _/docs_ folder.

## Hardware

Supported platforms : All ESP32 based board shall be supported.

## ToDo
 [] Add some unit tests

## Dependencies

nopnop2002 SSD1306 library (used in screen_mng)

## Roadmap

See the [open issues](https://gitlab.com/MOWA_/wirelessoctopus/issues) for a full list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Project licensed under [GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
