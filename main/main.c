/**
 * @file main.c
 * @author Cédric Jardin - MOWA ()
 * @brief Wireless Octopus firmware
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_mac.h"

#include "esp_flash_manager.h"
#include "esp_http_file_server.h"
#include "esp_wifi_manager.h"

#include "screen_mng.h"
#include "configuration_mng.h"
#include "bridge_mng.h"

#include "esp_log.h"
static const char *TAG = "MAIN";

void app_main(void)
{
    ESP_LOGI(TAG, "Starting Wireless Octopus...");
    
    /* Initialize screen manager */
    screen_mng_init(false);
    
    /* Initialize flash manager */
    t_esp_flash_manager_config flash_manager_config = {
        .use_external_flash = false,
        .ext_flash_config = DEFAULT_EXT_FLASH_CONFIGURATION,
        .base_path = "/flash",
        .max_open_files = 5,
        .format_if_mount_failed = true
    };
    esp_flash_manager_init(&flash_manager_config);

    /* Initialize configuration manager */
    configuration_mng_init();

    /* Initialize WiFi manager */
    t_esp_wifi_manager_config esp_wifi_manager_config = { 0 };
    esp_wifi_manager_config.sta.use_station = true;
    esp_wifi_manager_config.sta.auth_mode = WIFI_AUTH_WPA2_PSK;
    esp_wifi_manager_config.sta.max_attempts = 5;
    /* Fetch AP SSID and password from config manager */
    configuration_mng_get_sta_ap_ssid_password((char*)&esp_wifi_manager_config.sta.ssid, (char*)&esp_wifi_manager_config.sta.password);

    esp_wifi_manager_config.softap.use_softap = true;
    esp_wifi_manager_config.softap.auth_mode = WIFI_AUTH_WPA2_PSK;
    esp_wifi_manager_config.softap.max_clients = 5;
    /* Construct SoftAP SSID */
    uint8_t esp_mac_adress[6];
    esp_read_mac((uint8_t*)&esp_mac_adress, ESP_MAC_WIFI_SOFTAP);
    snprintf((char*)&esp_wifi_manager_config.softap.ssid, 32, "%s-%02X%02X%02X", "WIPUS",
                                                                esp_mac_adress[3],
                                                                esp_mac_adress[4],
                                                                esp_mac_adress[5]);
    strncpy(esp_wifi_manager_config.softap.password, "pwd12345678", sizeof(esp_wifi_manager_config.softap.password));
    esp_wifi_manager_init(&esp_wifi_manager_config);

    /* Initialize HTTP file server */
    t_esp_http_file_server_config s_esp_http_file_server_config = {
        .storage_root = "/flash",
        .max_file_size = 204800 //200KB
    };
    esp_http_file_server_init(&s_esp_http_file_server_config);

    /* Initialize bridge manager */
    bridge_mng_init();

    while (1) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        ESP_LOGI(TAG, "LOG - Heap free: %d", xPortGetFreeHeapSize());
    }
}
