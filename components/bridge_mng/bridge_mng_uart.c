/**
 * @file bridge_mng_uart.c
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge manager UART submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is meant to control activity of UART ports.
 * It can manage up to UART_MAX_NUM (defined by SOC) UART contexts.
 */

#include "bridge_mng_uart.h"

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_system.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_UART";

/* UART configuration defualts */
#define UART_RTS_PIN UART_PIN_NO_CHANGE
#define UART_CTS_PIN UART_PIN_NO_CHANGE

/* Maximum size for RX buffer */
#define RX_MAX_BUFFER_SIZE 1024

/**
 * @brief UART port context structure
 */
typedef struct{
    int32_t         uart_port_num;          /*!< UART port num */
    TaskHandle_t    uart_task_handle;       /*!< UART task handle */
    QueueHandle_t   tx_queue;               /*!< TX Message queue */
}t_bridge_mng_uart_ctx;
static t_bridge_mng_uart_ctx s_bridge_mng_uart_ctx[UART_NUM_MAX];

/*****************LOCALS*/
static void bridge_mng_uart_task( void *arg );

void bridge_mng_uart_init( void ){
    /* Setting up UART */
    ESP_LOGI(TAG, "Bridge manager submodule : UART..");

    memset(&s_bridge_mng_uart_ctx, 0, sizeof(s_bridge_mng_uart_ctx));
}

bool bridge_mng_uart_set_config( t_bridge_mng_uart_config* uart_config ){
    bool status = false;

    /* First, sanitize input parameters */
    if( uart_config->uart_num > UART_NUM_MAX || uart_config->uart_num < 0 ) return false;

    /* check if the UART task is already running */
    if( s_bridge_mng_uart_ctx[uart_config->uart_num].uart_task_handle != NULL ){
        /* Delete the running task */
        vTaskDelete(s_bridge_mng_uart_ctx[uart_config->uart_num].uart_task_handle);
        s_bridge_mng_uart_ctx[uart_config->uart_num].uart_task_handle = NULL;

        /* Delete associated TX queue */
        vQueueDelete(s_bridge_mng_uart_ctx[uart_config->uart_num].tx_queue);
    }

    /* Delete instanciated driver if it exists */
    if( uart_is_driver_installed(uart_config->uart_num) == true ){
        uart_driver_delete(uart_config->uart_num);
    }

    /* Instanciate UART with given configuration */
    uart_config_t uart_configuration = {
        .baud_rate = uart_config->baudrate,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
    };

    /* Install uart driver */
    ESP_ERROR_CHECK(uart_driver_install(uart_config->uart_num, RX_MAX_BUFFER_SIZE * 2, 0, 0, NULL, 0));

    /* Configure UART port */
    if( uart_param_config(uart_config->uart_num, &uart_configuration) == ESP_OK ){
        if( uart_set_pin(uart_config->uart_num, uart_config->tx_pin, uart_config->rx_pin, UART_RTS_PIN, UART_CTS_PIN) == ESP_OK ){
            /* Create queues */
            s_bridge_mng_uart_ctx[uart_config->uart_num].tx_queue = xQueueCreate( 5, MAXIMUM_LOG_LENGTH * sizeof(char) );
            assert(s_bridge_mng_uart_ctx[uart_config->uart_num].tx_queue != NULL);
            
            /* Save UART port num */
            s_bridge_mng_uart_ctx[uart_config->uart_num].uart_port_num = uart_config->uart_num;

            /* Create task and pass context as argument */
            xTaskCreate(bridge_mng_uart_task,
                        "bridge_mng_uart_task",
                        4096,
                        &s_bridge_mng_uart_ctx[uart_config->uart_num],
                        8,
                        &s_bridge_mng_uart_ctx[uart_config->uart_num].uart_task_handle);
            
            status = true;
        }
        else{
            ESP_LOGE(TAG, "Failed to set tx:%ld rx:%ld for UART port %ld", uart_config->tx_pin,
                                                                        uart_config->rx_pin,
                                                                        uart_config->uart_num);
            status = false;
        }
    }
    else{
        ESP_LOGE(TAG, "Failed to set configuration for UART port %ld", uart_config->uart_num);
        status = false;
    }
    
    return status;    
}

bool bridge_mng_uart_send_activity( t_activity_entry* activity_entry ){
    if( activity_entry->u.activity_uart.uart_port < UART_NUM_MAX
        && activity_entry->u.activity_uart.uart_port >= 0 ){
        if( s_bridge_mng_uart_ctx[activity_entry->u.activity_uart.uart_port].tx_queue != NULL ){
            if(xQueueSend(s_bridge_mng_uart_ctx[activity_entry->u.activity_uart.uart_port].tx_queue,
                    activity_entry->u.activity_uart.log, 10) != pdPASS){
                ESP_LOGW(TAG, "No space available into tx data queue");
                return false;
            }
            else{
                return true;
            }
        }
        else{
            ESP_LOGW(TAG, "UART is uninitialized.");
            return false;
        }
    }
    else{
        ESP_LOGW(TAG, "Parameters are incorrect.");
        return false;
    }
}

/***************************************LOCALS*/
static void bridge_mng_uart_task( void *arg ){
    /* Retrieve the UART context passed as argument */
    t_bridge_mng_uart_ctx* context = (t_bridge_mng_uart_ctx*)arg;

    /* Allocate a temporary buffer for rx data */
    uint8_t* data = (uint8_t*)malloc(RX_MAX_BUFFER_SIZE);

    while (1) {
        /* Check if something needs to be sent */
        char tx_data[MAXIMUM_LOG_LENGTH];
        if(xQueueReceive(context->tx_queue, &tx_data, 0) == pdPASS){
            /* Send received data through UART */
            uart_write_bytes(context->uart_port_num, (const char *)&tx_data, strlen(tx_data));
        }

        /* Read data from UART */
        int len = uart_read_bytes(context->uart_port_num, data, (RX_MAX_BUFFER_SIZE - 1), 20 / portTICK_PERIOD_MS);
        
        if (len) {
            data[len] = '\0';

            /* Create new activity entry */
            t_activity_entry activity_entry;
            memset(&activity_entry, 0, sizeof(t_activity_entry));
            
            activity_entry.activity_type = ACTIVITY_TYPE_UART;
            activity_entry.activity_origin = ACTIVITY_ORIGIN_EMBEDDED;
            activity_entry.u.activity_uart.uart_port = context->uart_port_num;
            snprintf(activity_entry.u.activity_uart.log, MAXIMUM_LOG_LENGTH, (char *) data);

            bridge_mng_add_activity_entry(&activity_entry);
        }
    }
}