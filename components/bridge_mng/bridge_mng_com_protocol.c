/**
 * @file bridge_mng_com_protocol.c
 * @author CJ (c.jardin@gys.fr)
 * @brief Bridge manager external communication protocol
 * @version 0.1
 * @date 2022-11-29
 * 
 * Custom protocol handling for communication.
 * Based on JSON 
 * 
 * @copyright Copyright (c) 2022
 */

#include "bridge_mng_com_protocol.h"
#include "bridge_mng_uart.h"
#include "bridge_mng_gpio.h"
#include "configuration_mng.h"

#include "string.h"
#include "esp_system.h"
#include "esp_timer.h"
#include "cJSON.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_COM";

#define MAX_COMMAND_OUTPUT_LENGTH 100 // Maximum string length for a command output

/**
 * @brief Enum of supported commands
 */
typedef enum{
    COMMAND_RESTART,
    COMMAND_SET_WIFI_STA_SSID,
    COMMAND_SET_WIFI_STA_PASSWORD,
    COMMAND_SET_CONFIGURATION_UART,
    COMMAND_SEND_DATA_UART,
    COMMAND_SET_GPIO_AS_DIGITAL_OUTPUT,
    COMMAND_SET_GPIO_AS_DIGITAL_INPUT,
    COMMAND_SET_GPIO_STATE_DIGITAL_OUTPUT,
    COMMAND_SET_GPIO_AS_ANALOG_INPUT,
    COMMAND_MAX
}e_com_server_protocol_command;

/**
 * @brief Commands name
 */
static const char* a_com_protocol_command_name[COMMAND_MAX] = {
    "restart",
    "set_wifi_sta_ssid",
    "set_wifi_sta_password",
    "set_configuration_uart",
    "send_data_uart",
    "set_gpio_as_digital_output",
    "set_gpio_as_digital_input",
    "set_gpio_state_digital_output",
    "set_gpio_as_analog_input"
};

/**
 * @brief Callback prototype for communication server commands
 */
typedef bool (*com_protocol_command_cb)( cJSON* parameter, char* output );

static bool cb_command_restart( cJSON* parameter, char* output );
static bool cb_command_set_wifi_sta_ssid( cJSON* parameter, char* output );
static bool cb_command_set_wifi_sta_password( cJSON* parameter, char* output );
static bool cb_command_set_configuration_uart( cJSON* parameter, char* output );
static bool cb_command_send_data_uart( cJSON* parameter, char* output );
static bool cb_command_set_gpio_as_digital_output( cJSON* parameter, char* output );
static bool cb_command_set_gpio_as_digital_input( cJSON* parameter, char* output );
static bool cb_command_set_gpio_state_digital_output( cJSON* parameter, char* output );
static bool cb_command_set_gpio_as_analog_input( cJSON* parameter, char* output );

/**
 * @brief Commands callback
 */
static const com_protocol_command_cb a_com_protocol_command_cb[COMMAND_MAX] = {
    cb_command_restart,
    cb_command_set_wifi_sta_ssid,
    cb_command_set_wifi_sta_password,
    cb_command_set_configuration_uart,
    cb_command_send_data_uart,
    cb_command_set_gpio_as_digital_output,
    cb_command_set_gpio_as_digital_input,
    cb_command_set_gpio_state_digital_output,
    cb_command_set_gpio_as_analog_input
};

/**************LOCALS*/
static void local_restart_timer_callback( void* arg );

char* bridge_mng_com_protocol_handle_data( char* data, uint32_t data_length ){
    cJSON* received_json = NULL;
    cJSON* response_json = NULL;
    char* response_buffer = NULL;
    bool command_result = false;
    char command_output[MAX_COMMAND_OUTPUT_LENGTH] = { 0 };

    /* Clear command output buffer */
    memset(command_output, 0, sizeof(command_output));

    /* Create JSON response object */
    response_json = cJSON_CreateObject();
    if( response_json == NULL ){
        ESP_LOGW(TAG, "Not enough memory to allocate JSON object, abort..");
        return NULL;
    }

    received_json = cJSON_ParseWithLength(data, data_length);
    if( received_json != NULL ){
        /* Check for command property */
        if(cJSON_GetObjectItem(received_json, "command")){
            for(uint16_t idx = 0; idx < COMMAND_MAX; idx++){
                /* Try to find the requested command */
                if(strncasecmp((char*)cJSON_GetObjectItem(received_json, "command")->valuestring,
                    a_com_protocol_command_name[idx],
                    strlen(a_com_protocol_command_name[idx])) == 0){
                    /* Call the callback associated to the command */
                    if( a_com_protocol_command_cb[idx] != NULL ){
                        /* If parameter is given with the request, pass it to the callback */
                        if(cJSON_GetObjectItem(received_json, "parameter"))
                            command_result = a_com_protocol_command_cb[idx](
                                    (cJSON*)cJSON_GetObjectItem(received_json, "parameter"),
                                    (char*)&command_output);
                        else
                            command_result = a_com_protocol_command_cb[idx](NULL, (char*)&command_output);
                    }      
                }
            }
        }
        else{
            /* No command to process, abort */
            command_result = false;
        }
    }
    else{
        command_result = false;
        ESP_LOGW(TAG, "Either there is not enough memory to process the data or given data is not a JSON object..");
    }

    /* Send command response back */
    cJSON_AddBoolToObject(response_json, "command_result", command_result);
    cJSON_AddStringToObject(response_json, "command_output", (char*)&command_output);
        
    response_buffer = cJSON_Print(response_json);

    /* Deallocate memory */
    if( received_json != NULL ) cJSON_Delete(received_json);
    if( response_json != NULL ) cJSON_Delete(response_json);

    return response_buffer;
}

char* bridge_mng_com_protocol_format_activity_entry( t_activity_entry* activity_entry ){
    cJSON* response_json = NULL;
    char* response_buffer = NULL;

    /* Create JSON response object */
    response_json = cJSON_CreateObject();
    if( response_json == NULL ){
        ESP_LOGW(TAG, "Not enough memory to allocate JSON object, abort..");
        return NULL;
    }

    /* Format activity entry according to its type */
    cJSON_AddNumberToObject(response_json, "activity_type", activity_entry->activity_type);

    switch(activity_entry->activity_type){
        case ACTIVITY_TYPE_UART:
        {
            cJSON_AddStringToObject(response_json, "log", (char*)activity_entry->u.activity_uart.log);
        }
        break;

        case ACTIVITY_TYPE_GPIO:
        {
            cJSON_AddNumberToObject(response_json, "pin_num", activity_entry->u.activity_gpio.pin_number);

            if( activity_entry->u.activity_gpio.pin_type == false ){
                cJSON_AddNumberToObject(response_json, "last_state", activity_entry->u.activity_gpio.last_state);
                cJSON_AddNumberToObject(response_json, "current_state", activity_entry->u.activity_gpio.current_state);
            }
            else{
                cJSON_AddNumberToObject(response_json, "last_value", activity_entry->u.activity_gpio.last_value);
                cJSON_AddNumberToObject(response_json, "current_value", activity_entry->u.activity_gpio.current_value);
            }
        }
        break;

        default:
            assert(0);
        break;
    }
        
    response_buffer = cJSON_Print(response_json);

    /* Deallocate memory */
    if( response_json != NULL ) cJSON_Delete(response_json);

    return response_buffer;
}

/**************LOCALS*/

/**
 * @brief Timer callback used to trigger esp restart.
 */
static void local_restart_timer_callback( void* arg ){
    esp_restart();
}

/*******************CALLBACKS*/
static bool cb_command_restart( cJSON* parameter, char* output ){
    /* Create a 5s one shot timer to trigger esp_restart and be able to respond. */
    const esp_timer_create_args_t restart_timer_args = {
            .callback = &local_restart_timer_callback,
            .name = "restart_os"
    };
    esp_timer_handle_t restart_timer;
    ESP_ERROR_CHECK(esp_timer_create(&restart_timer_args, &restart_timer));
    ESP_ERROR_CHECK(esp_timer_start_once(restart_timer, 5000000));

    snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "restarting in 5 seconds");
    return true;
}

static bool cb_command_set_wifi_sta_ssid( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsString(parameter) == true ){
        if( strlen(parameter->valuestring) < CONFIG_MAX_WIFI_AP_SSID_LENGTH ){
            configuration_mng_set_sta_ap_ssid(parameter->valuestring);
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
            return true;
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ssid max length : %d", CONFIG_MAX_WIFI_AP_SSID_LENGTH);
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_wifi_sta_password( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsString(parameter) == true ){
        if( strlen(parameter->valuestring) < CONFIG_MAX_WIFI_PASSWORD_LENGTH ){
            configuration_mng_set_sta_ap_password(parameter->valuestring);
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
            return true;
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "password max length : %d", CONFIG_MAX_WIFI_PASSWORD_LENGTH);
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_configuration_uart( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsObject(parameter) == true ){
        /* Check for mandatory parameters */
        if( cJSON_HasObjectItem(parameter, "uart_port_num") 
            && cJSON_HasObjectItem(parameter, "baudrate")
            && cJSON_HasObjectItem(parameter, "rx_pin")
            && cJSON_HasObjectItem(parameter, "tx_pin")
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "uart_port_num"))
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "baudrate"))
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "rx_pin"))
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "tx_pin")) ){

            /* Configure UART with given configuration */
            t_bridge_mng_uart_config s_bridge_mng_uart_config = { 0 };
            s_bridge_mng_uart_config.uart_num = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "uart_port_num"));
            s_bridge_mng_uart_config.baudrate = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "baudrate"));
            s_bridge_mng_uart_config.rx_pin = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "rx_pin"));
            s_bridge_mng_uart_config.tx_pin = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "tx_pin"));
            
            if( bridge_mng_uart_set_config(&s_bridge_mng_uart_config) == true ){
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
                return true;
            }
            else{
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to configure UART");
                return false;
            }
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "wrong parameters");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_send_data_uart( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsObject(parameter) == true ){
        /* Check for mandatory parameters */
        if( cJSON_HasObjectItem(parameter, "uart_port_num") 
            && cJSON_HasObjectItem(parameter, "log")
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "uart_port_num"))
            && cJSON_IsString(cJSON_GetObjectItem(parameter, "log")) ){

            /* Construct new activity and send it */
            t_activity_entry s_activity_entry;
            s_activity_entry.activity_origin = ACTIVITY_ORIGIN_EXTERIOR;
            s_activity_entry.activity_type = ACTIVITY_TYPE_UART;
            s_activity_entry.u.activity_uart.uart_port = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "uart_port_num"));
            strlcpy(s_activity_entry.u.activity_uart.log, cJSON_GetStringValue(cJSON_GetObjectItem(parameter, "log")), MAXIMUM_LOG_LENGTH);

            if(bridge_mng_add_activity_entry(&s_activity_entry) == true){
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
                return true;
            }
            else{
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to send");
                return false;
            }
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "wrong parameters");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_gpio_as_digital_output( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsNumber(parameter) == true ){
        if( bridge_mng_gpio_configure_as_digital_output(parameter->valueint) == true ){
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
            return true;
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to set pin as digital output");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_gpio_as_digital_input( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsNumber(parameter) == true ){
        if( bridge_mng_gpio_configure_as_digital_input(parameter->valueint) == true ){
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
            return true;
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to set pin as digital input");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_gpio_state_digital_output( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsObject(parameter) == true ){
        /* Check for mandatory parameters */
        if( cJSON_HasObjectItem(parameter, "pin_num") 
            && cJSON_HasObjectItem(parameter, "state")
            && cJSON_IsNumber(cJSON_GetObjectItem(parameter, "pin_num"))
            && cJSON_IsBool(cJSON_GetObjectItem(parameter, "state")) ){
            /* Construct activity to send it via bridge_mng */
            t_activity_entry s_activity_entry;
            s_activity_entry.activity_origin = ACTIVITY_ORIGIN_EXTERIOR;
            s_activity_entry.activity_type = ACTIVITY_TYPE_GPIO;
            s_activity_entry.u.activity_gpio.pin_number = cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "pin_num"));
            s_activity_entry.u.activity_gpio.pin_type = false;
            s_activity_entry.u.activity_gpio.direction = true;
            s_activity_entry.u.activity_gpio.last_state = (bool)cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "state"));
            s_activity_entry.u.activity_gpio.current_state = (bool)cJSON_GetNumberValue(cJSON_GetObjectItem(parameter, "state"));
            
            if( bridge_mng_add_activity_entry(&s_activity_entry) == true ){
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
                return true;
            }
            else{
                snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to set pin state");
                return false;
            }
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "wrong parameters");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}

static bool cb_command_set_gpio_as_analog_input( cJSON* parameter, char* output ){
    if( parameter != NULL && cJSON_IsNumber(parameter) == true ){
        if( bridge_mng_gpio_configure_as_analog_input(parameter->valueint) == true ){
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "ok");
            return true;
        }
        else{
            snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "failed to set pin as analog input");
            return false;
        }
    }
    else{
        snprintf(output, MAX_COMMAND_OUTPUT_LENGTH, "parameter is required");
        return false;
    }
}
