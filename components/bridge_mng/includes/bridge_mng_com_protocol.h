/**
 * @file bridge_mng_com_protocol.h
 * @author CJ (c.jardin@gys.fr)
 * @brief Bridge manager external communication protocol
 * @version 0.1
 * @date 2022-11-29
 * 
 * Custom protocol handling for communication.
 * Based on JSON 
 * 
 * @copyright Copyright (c) 2022
 */

#ifndef __BRIDGE_MNG_COM_PROTOCOL_H__
#define __BRIDGE_MNG_COM_PROTOCOL_H__

#include <stdio.h>
#include <stdbool.h>

#include "bridge_mng.h"

/**
 * @brief Function used to handles incoming request (JSON)
 * and respond to it.
 * 
 * @param data Incoming request buffer
 * @param data_length Buffer length
 * @return char* Response buffer (Must be freed after use)
 */
char* bridge_mng_com_protocol_handle_data( char* data, uint32_t data_length );

/**
 * @brief Function used to format activity entry before sending it to clients
 * 
 * @param activity_entry Activity entry
 * @return char* Formatted activity entry (Must be free after use)
 */
char* bridge_mng_com_protocol_format_activity_entry( t_activity_entry* activity_entry );

#endif //__BRIDGE_MNG_COM_PROTOCOL_H__
