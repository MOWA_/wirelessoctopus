/**
 * @file bridge_mng_store.h
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge management store submodule
 * @version 0.1
 * @date 2023-05-15
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is in charge of storing incoming activity into a filesystem.
 */

#ifndef __BRIDGE_MNG_STORE_H__
#define __BRIDGE_MNG_STORE_H__

#include <stdint.h>
#include "bridge_mng.h"

/**
 * @brief Initialize store submodule.
 */
void bridge_mng_store_init( void );

/**
 * @brief Function used to store incoming activity.
 * 
 * @param activity_entry Activity entry
 */
void bridge_mng_store( t_activity_entry* activity_entry );

#endif //__BRIDGE_MNG_STORE_H__
