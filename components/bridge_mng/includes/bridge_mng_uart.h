/**
 * @file bridge_mng_uart.c
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge manager UART submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is meant to control activity of UART ports.
 * It can manage up to UART_MAX_NUM (defined by SOC) UART contexts.
 */

#ifndef __BRIDGE_MNG_UART_H__
#define __BRIDGE_MNG_UART_H__

#include "stdint.h"
#include "stdbool.h"
#include "bridge_mng.h"

/**
 * @brief Initialize bridge manager UART component.
 * 
 * This function handles UART initialization.
 */
void bridge_mng_uart_init( void );

/**
 * @brief Bridge manager UART configuration structure
 */
typedef struct{
    int32_t     uart_num;       /*!< UART port num */
    uint32_t    baudrate;       /*!< UART baudrate */
    int32_t     rx_pin;         /*!< UART RX pin */
    int32_t     tx_pin;         /*!< UART TX pin */
}t_bridge_mng_uart_config;

/**
 * @brief Function used to configure UART with the specified config.
 * 
 * @param uart_config UART configuration
 */
bool bridge_mng_uart_set_config( t_bridge_mng_uart_config* uart_config );

/**
 * @brief Send activity content to UART.
 * 
 * @param activity_entry Activity entry
 */
bool bridge_mng_uart_send_activity( t_activity_entry* activity_entry );

#endif //__BRIDGE_MNG_UART_H__
