/**
 * @file bridge_mng_gpio.h
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge manager GPIO submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is meant to control activity GPIO pins.
 */

#ifndef __BRIDGE_MNG_GPIO_H__
#define __BRIDGE_MNG_GPIO_H__

#include "stdint.h"
#include "stdbool.h"
#include "bridge_mng.h"

/**
 * @brief Initialize bridge manager GPIO component.
 * 
 * This function handles GPIO submodule initialization.
 */
void bridge_mng_gpio_init( void );

/**
 * @brief Send activity content to GPIO submodule.
 * 
 * @param activity_entry Activity entry
 */
bool bridge_mng_gpio_send_activity( t_activity_entry* activity_entry );

/**
 * @brief Configure a pin as digital input.
 * User must ensure that pin is not already in use as it may fail or panic.
 * 
 * @param pin_num Pin number
 */
bool bridge_mng_gpio_configure_as_digital_input( uint32_t pin_num );

/**
 * @brief Configure a pin as digital output.
 * User must ensure that pin is not already in use as it may fail or panic.
 * 
 * @param pin_num Pin number
 */
bool bridge_mng_gpio_configure_as_digital_output( uint32_t pin_num );

/**
 * @brief Configure a pin as analog input.
 * User must ensure that pin is not already in use as it may fail or panic.
 * 
 * @param pin_num Pin number
 */
bool bridge_mng_gpio_configure_as_analog_input( uint32_t pin_num );

#endif //__BRIDGE_MNG_GPIO_H__
