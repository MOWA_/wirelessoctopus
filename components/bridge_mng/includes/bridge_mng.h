/**
 * @file bridge_mng.h
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge management module
 * @version 0.1
 * @date 2023-05-15
 * 
 * @copyright Copyright (c) 2023
 * 
 * This module is the main component of the project.
 * It acts as a bridge between the embedded system and the exterior world.
 */

#ifndef __BRIDGE_MNG_H__
#define __BRIDGE_MNG_H__

#include "stdint.h"
#include "stdbool.h"

#define MAXIMUM_LOG_LENGTH 200  /* Maximum log length */

/**
 * @brief Enum of activity types supported by the bridge manager
 */
typedef enum{
     ACTIVITY_TYPE_UART
    ,ACTIVITY_TYPE_GPIO
    ,ACTIVITY_TYPE_NB
}e_activity_type;

/**
 * @brief Enum of activity origin
 */
typedef enum{
     ACTIVITY_ORIGIN_EMBEDDED
    ,ACTIVITY_ORIGIN_EXTERIOR
    ,ACTIVITY_ORIGIN_NB
}e_activity_origin;

/**
 * @brief UART activity structure
 */
typedef struct{
    int32_t     uart_port;                  /*!< UART port */
    char        log[MAXIMUM_LOG_LENGTH];    /*!< Log buffer */
}t_activity_uart;

/**
 * @brief GPIO activity structure
 */
typedef struct{
    uint32_t    pin_number;                 /*!< GPIO Pin number */
    bool        pin_type;                   /*!< GPIO Pin type - 0: digital, 1: analog */
    bool        direction;                  /*!< GPIO Pin direction - 0: input, 1: output */

    bool        last_state;                 /*!< GPIO Pin last state (applicable when digital) */
    bool        current_state;              /*!< GPIO Pin current state (applicable when digital) */
    
    uint16_t    last_value;                 /*!< GPIO Pin last value (applicable when analog) */
    uint16_t    current_value;              /*!< GPIO Pin current value (applicable when analog) */
}t_activity_gpio;

/**
 * @brief Activity entry structure
 */
typedef struct{
    e_activity_origin   activity_origin;        /*!< Activity origin */
    e_activity_type     activity_type;          /*!< Activity type */
    union{
        t_activity_uart activity_uart;          /*!< UART typed activity */
        t_activity_gpio activity_gpio;          /*!< GPIO typed activity */
    }u;
}t_activity_entry;

/**
 * @brief Initialize bridge manager.
 * 
 * This function handles bridge manager initialization.
 */
void bridge_mng_init( void );

/**
 * @brief Add a new activity entry.
 * 
 * @param activity_entry Activity entry
 */
bool bridge_mng_add_activity_entry( t_activity_entry* activity_entry );

#endif //__BRIDGE_MNG_H__
