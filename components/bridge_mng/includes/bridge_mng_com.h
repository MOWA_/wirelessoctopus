/**
 * @file bridge_mng_com.h
 * @author CJ (c.jardin@gys.fr)
 * @brief Bridge manager external communcation submodule
 * @version 0.1
 * @date 2022-11-29
 * 
 * TCP server to handle external communication
 * 
 * @copyright Copyright (c) 2022
 */

#ifndef __BRIDGE_MNG_COM_H__
#define __BRIDGE_MNG_COM_H__

#include <stdio.h>
#include <stdbool.h>

#include "bridge_mng.h"

/**
 * @brief Initialize bridge communication server.
 * 
 * This function handles TCP communication server initialization.
 */
void bridge_mng_com_init( void );

/**
 * @brief Function used to receive new activity entries to send it to connected clients
 * 
 * @param activity_entry Activity entry
 */
bool bridge_mng_com_new_entry( t_activity_entry* activity_entry );

#endif //__BRIDGE_MNG_COM_H__
