/**
 * @file bridge_mng_com.c
 * @author CJ (c.jardin@gys.fr)
 * @brief Bridge management external communcation submodule
 * @version 0.1
 * @date 2022-11-29
 * 
 * TCP server to handle external communication
 * 
 * @copyright Copyright (c) 2022
 */

#include "bridge_mng_com.h"
#include "bridge_mng_com_protocol.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "errno.h"
#include "sys/socket.h"
#include "netdb.h"
#include "esp_mac.h"
#include "cJSON.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_COM";

#define SERVER_MAX_OPEN_SOCKETS             3                       /*!< Server maximum number of opened sockets */
#define SERVER_AVAILABLE_SOCKET_MARKER      (-1)                    /*!< Server socket available marker */

/* Server listening socket hints */
static const struct addrinfo server_listening_socket_hints = { .ai_socktype = SOCK_STREAM };

/**
 * @brief Bridge manager com context
 */
typedef struct{
    int32_t                     server_sockets[SERVER_MAX_OPEN_SOCKETS];    /*!< Server sockets */

    struct addrinfo*            server_listening_socket_infos;              /*!< Server listening socket info */
    int32_t                     server_listening_socket;                    /*!< Server listening socket */

    QueueHandle_t               activity_entries_queue;                     /*!< Activity entries queue */
}t_bridge_mng_com_ctx;

/* Bridge manager com context */
static t_bridge_mng_com_ctx s_bridge_mng_com_ctx;

/******************LOCALS/UTILS*/
static void local_bridge_mng_com_tcp_task( void* pvParameters );
static int32_t local_server_try_receive( const int32_t socket, char* data, size_t max_len );
static int32_t local_server_socket_send(const int socket, const char* data, const size_t len);

static void local_server_log_socket_error( const int sock, const int err, const char *message );

void bridge_mng_com_init( void ){
    ESP_LOGI(TAG, "Bridge manager submodule : com..");

    memset(&s_bridge_mng_com_ctx, 0, sizeof(t_bridge_mng_com_ctx));

    /* Create queues */
    s_bridge_mng_com_ctx.activity_entries_queue = xQueueCreate( 5, sizeof(t_activity_entry) );
    assert(s_bridge_mng_com_ctx.activity_entries_queue != NULL);

    xTaskCreate(local_bridge_mng_com_tcp_task, "com_server", 4096, NULL, 5, NULL);
}

bool bridge_mng_com_new_entry( t_activity_entry* activity_entry ){
    if(xQueueSend(s_bridge_mng_com_ctx.activity_entries_queue, activity_entry, 10) != pdPASS){
        ESP_LOGW(TAG, "No space available into activity entry queue");
        return false;
    }
    else{
        return true;
    }
}
/**************************LOCALS*/
/**
 * @brief Main bridge mng com task
 * 
 * @param pvParameters 
 */
static void local_bridge_mng_com_tcp_task( void* pvParameters ){
    static char rx_buffer[256];
    uint8_t v_idx = 0;
    t_activity_entry activity_entry;

    /* Prepare all server sockets, marking them as available */
    for(uint8_t i = 0; i < SERVER_MAX_OPEN_SOCKETS; i++){
        s_bridge_mng_com_ctx.server_sockets[i] = SERVER_AVAILABLE_SOCKET_MARKER;
    }

    /* Translating the hostname or string representation of the IP to address_info */
    getaddrinfo("0.0.0.0", "9000", &server_listening_socket_hints, &s_bridge_mng_com_ctx.server_listening_socket_infos);

    /* Create the server listening socket */
    s_bridge_mng_com_ctx.server_listening_socket = socket(s_bridge_mng_com_ctx.server_listening_socket_infos->ai_family,
                                                    s_bridge_mng_com_ctx.server_listening_socket_infos->ai_socktype,
                                                    s_bridge_mng_com_ctx.server_listening_socket_infos->ai_protocol);
    assert( s_bridge_mng_com_ctx.server_listening_socket >= 0 );

    ESP_LOGI(TAG, "Server listening socket created");

    /* Mark the socket as non blocking */
    int32_t listening_socket_flags = fcntl(s_bridge_mng_com_ctx.server_listening_socket, F_GETFL);
    if(fcntl(s_bridge_mng_com_ctx.server_listening_socket, F_SETFL, listening_socket_flags | O_NONBLOCK) == -1) {
        local_server_log_socket_error(s_bridge_mng_com_ctx.server_listening_socket, errno, "Unable to set socket non blocking");
        assert(0);
    }
    ESP_LOGI(TAG, "Socket marked as non blocking");

    /* Bind listening socket to the given address */
    if(bind(s_bridge_mng_com_ctx.server_listening_socket, s_bridge_mng_com_ctx.server_listening_socket_infos->ai_addr, s_bridge_mng_com_ctx.server_listening_socket_infos->ai_addrlen) != 0) {
        local_server_log_socket_error(s_bridge_mng_com_ctx.server_listening_socket, errno, "Socket unable to bind");
        assert(0);
    }

    /* Set queue (backlog) of pending connections to one (can be more) */
    if(listen(s_bridge_mng_com_ctx.server_listening_socket, 1) != 0) {
        local_server_log_socket_error(s_bridge_mng_com_ctx.server_listening_socket, errno, "Error occurred during listen");
        assert(0);
    }
    ESP_LOGI(TAG, "Socket listening");

    /* Main loop for accepting new connections and serving all connected clients */
    while (1) {
        struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
        socklen_t addr_len = sizeof(source_addr);

        /* Find an available socket for a new client */
        int32_t v_socket_idx = 0;
        for(v_socket_idx = 0; v_socket_idx < SERVER_MAX_OPEN_SOCKETS; v_socket_idx++){
            if(s_bridge_mng_com_ctx.server_sockets[v_socket_idx] == SERVER_AVAILABLE_SOCKET_MARKER) {
                break;
            }
        }

        /* Try to accept a connection only if maximum connected clients hasn't been reached */
        if(v_socket_idx < SERVER_MAX_OPEN_SOCKETS){
            /* Try to accept a new connection (non blocking) */
            s_bridge_mng_com_ctx.server_sockets[v_socket_idx] = accept(s_bridge_mng_com_ctx.server_listening_socket, (struct sockaddr *)&source_addr, &addr_len);

            /* If no connection has been accepted, check if it's an error or not */
            if(s_bridge_mng_com_ctx.server_sockets[v_socket_idx] < 0){
                if(errno == EWOULDBLOCK){
                    ESP_LOGV(TAG, "No pending connections...");
                } else {
                    local_server_log_socket_error(s_bridge_mng_com_ctx.server_listening_socket, errno, "Error when accepting connection");
                    assert(0);
                }
            }
            else{
                /* New client connected */
                ESP_LOGI(TAG, "[sock=%ld]: Connection accepted", s_bridge_mng_com_ctx.server_sockets[v_socket_idx]);

                /* Set client socket as non-blocking */
                int32_t flags = fcntl(s_bridge_mng_com_ctx.server_sockets[v_socket_idx], F_GETFL);
                if (fcntl(s_bridge_mng_com_ctx.server_sockets[v_socket_idx], F_SETFL, flags | O_NONBLOCK) == -1) {
                    local_server_log_socket_error(s_bridge_mng_com_ctx.server_sockets[v_socket_idx], errno, "Unable to set socket non blocking");
                    assert(0);
                }

                ESP_LOGI(TAG, "[sock=%ld]: Socket marked as non blocking", s_bridge_mng_com_ctx.server_sockets[v_socket_idx]);
            }
        }

        /* Serve all connected clients */
        /* If something needs to be sent, send it to all connected clients */
        if(xQueueReceive(s_bridge_mng_com_ctx.activity_entries_queue, &activity_entry, 0) == pdPASS){
            char* send_buffer = bridge_mng_com_protocol_format_activity_entry(&activity_entry);

            for(v_idx = 0; v_idx < SERVER_MAX_OPEN_SOCKETS; v_idx++){
                /* If socket is opened */
                if(s_bridge_mng_com_ctx.server_sockets[v_idx] != SERVER_AVAILABLE_SOCKET_MARKER){
                    int32_t len = local_server_socket_send( s_bridge_mng_com_ctx.server_sockets[v_idx], send_buffer, strlen(send_buffer));
                    
                    if(len < 0){
                        /* Error occurred on write to this socket, close and mark it available */
                        ESP_LOGI(TAG, "[sock=%ld]: local_server_socket_send() returned %ld -> closing the socket", s_bridge_mng_com_ctx.server_sockets[v_idx], len);
                    
                        close(s_bridge_mng_com_ctx.server_sockets[v_idx]);
                        s_bridge_mng_com_ctx.server_sockets[v_idx] = SERVER_AVAILABLE_SOCKET_MARKER;
                    } else {
                        // Successfully wrote to this socket
                        ESP_LOGI(TAG, "[sock=%ld]: Written", s_bridge_mng_com_ctx.server_sockets[v_idx]);
                    }
                }
            }

            free(send_buffer);
        }

        /* Handle requests */
        for(uint8_t v_idx = 0; v_idx < SERVER_MAX_OPEN_SOCKETS; v_idx++){
            /* If socket is opened */
            if(s_bridge_mng_com_ctx.server_sockets[v_idx] != SERVER_AVAILABLE_SOCKET_MARKER){
                /* Try to receive data from socket */
                int32_t len = local_server_try_receive(s_bridge_mng_com_ctx.server_sockets[v_idx], rx_buffer, sizeof(rx_buffer));

                /* Error occurred while trying to read from the socket, close and mark it available */
                if (len < 0) {  
                    ESP_LOGI(TAG, "[sock=%ld]: local_server_try_receive() returned %ld -> closing the socket", s_bridge_mng_com_ctx.server_sockets[v_idx], len);
                    
                    close(s_bridge_mng_com_ctx.server_sockets[v_idx]);
                    s_bridge_mng_com_ctx.server_sockets[v_idx] = SERVER_AVAILABLE_SOCKET_MARKER;
                }
                else if(len > 0){
                    /* Received some data -> respond */
                    ESP_LOGI(TAG, "[sock=%ld]: Received", s_bridge_mng_com_ctx.server_sockets[v_idx]);

                    /* Handle received data */
                    char* send_buffer = bridge_mng_com_protocol_handle_data(rx_buffer, len);
                    len = local_server_socket_send( s_bridge_mng_com_ctx.server_sockets[v_idx], send_buffer, strlen(send_buffer));
                    
                    if(len < 0){
                        /* Error occurred on write to this socket, close and mark it available */
                        ESP_LOGI(TAG, "[sock=%ld]: local_server_socket_send() returned %ld -> closing the socket", s_bridge_mng_com_ctx.server_sockets[v_idx], len);
                    
                        close(s_bridge_mng_com_ctx.server_sockets[v_idx]);
                        s_bridge_mng_com_ctx.server_sockets[v_idx] = SERVER_AVAILABLE_SOCKET_MARKER;
                    } else {
                        // Successfully wrote to this socket
                        ESP_LOGI(TAG, "[sock=%ld]: Written", s_bridge_mng_com_ctx.server_sockets[v_idx]);
                    }

                    vPortFree(send_buffer);
                }
            }
        }

        // Give CPU hand
        vTaskDelay(pdMS_TO_TICKS(50));
    }
}

/**
 * @brief Tries to receive data from specified sockets in a non-blocking way,
 *        i.e. returns immediately if no data.
 *
 * @param[in] sock Socket for reception
 * @param[out] data Data pointer to write the received data
 * @param[in] max_len Maximum size of the allocated space for receiving data
 * @return
 *          >0 : Size of received data
 *          =0 : No data available
 *          -1 : Error occurred during socket read operation
 *          -2 : Socket is not connected, to distinguish between an actual socket error and active disconnection
 */
static int32_t local_server_try_receive( const int32_t socket, char* data, size_t max_len ){
    int32_t len = recv(socket, data, max_len, 0);
    if(len < 0){
        if(errno == EINPROGRESS || errno == EAGAIN || errno == EWOULDBLOCK){
            // Not an error
            return 0;   
        }
        if(errno == ENOTCONN){
            // Socket has been disconnected
            ESP_LOGW(TAG, "[sock=%ld]: Connection closed", socket);

            return -2;  
        }
        local_server_log_socket_error(socket, errno, "Error occurred during receiving");

        return -1;
    }

    return len;
}

/**
 * @brief Sends the specified data to the socket. This function blocks until all bytes got sent.
 *
 * @param[in] sock Socket to write data
 * @param[in] data Data to be written
 * @param[in] len Length of the data
 * @return
 *          >0 : Size the written data
 *          -1 : Error occurred during socket write operation
 */
static int32_t local_server_socket_send(const int socket, const char* data, const size_t len){
    int32_t data_size_to_write = len;
    while (data_size_to_write > 0) {
        int32_t written = send(socket, data + (len - data_size_to_write), data_size_to_write, 0);

        if (written < 0 && errno != EINPROGRESS && errno != EAGAIN && errno != EWOULDBLOCK) {
            local_server_log_socket_error(socket, errno, "Error occurred during sending");
            return -1;
        }

        data_size_to_write -= written;
    }

    return len;
}

/***********************************LOCAL UTILS*/
/**
 * @brief Utility to log socket errors
 *
 * @param[in] sock Socket number
 * @param[in] err Socket errno
 * @param[in] message Message to print
 */
static void local_server_log_socket_error( const int sock, const int err, const char *message ){
    ESP_LOGE(TAG, "[sock = %d]: %s\n"
                  "error = %d: %s", sock, message, err, strerror(err));
}
