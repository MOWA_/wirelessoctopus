/**
 * @file bridge_mng_store.c
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge management store submodule
 * @version 0.1
 * @date 2023-05-15
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is in charge of storing incoming activity into a filesystem.
 */

#include "bridge_mng_store.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <sys/stat.h>
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_STORE";

/* File name for every activity types */
static const char* a_activity_type_file_name[ACTIVITY_TYPE_NB] = {
    "/flash/UART.log",
    "/flash/GPIO.log"
};

/* Maximum activity entries in log files */
#define MAXIMUM_ENTRIES_IN_LOG_FILES 10

/* Activity entry queue */
static QueueHandle_t activity_entry_queue_handle = NULL;

/* Bridge manager store submodule task handle */
static TaskHandle_t store_task_handle = NULL;

/***************************************LOCALS*/
static void bridge_mng_store_task( void *arg );
static void local_remove_carriage_return( char* buffer );
static uint32_t local_get_timestamp( void );

void bridge_mng_store_init( void ){
    ESP_LOGI(TAG, "Bridge manager submodule : store..");

    /* Create activity entry queue */
    activity_entry_queue_handle = xQueueCreate(5, sizeof(t_activity_entry));
    assert(activity_entry_queue_handle != NULL);

    /* Create store submodule task */
    xTaskCreate(bridge_mng_store_task, "bridge_mng_store_task", 4096, NULL, 5, &store_task_handle);
    assert(store_task_handle != NULL);
}

void bridge_mng_store( t_activity_entry* activity_entry ){
    /* Send activity entry to queue */
    if( xQueueSend(activity_entry_queue_handle, activity_entry, portMAX_DELAY) != pdTRUE ){
        ESP_LOGI(TAG, "Failed to add entry to queue..");
        assert(0);
    }
}

/***************************************LOCALS*/
static void bridge_mng_store_task( void *arg ){
    t_activity_entry activity_entry = { 0 };
    QueueHandle_t tmp_queue = NULL;
    FILE* file = NULL;
    char line[200] = { 0 };

    while(1){
        /* Dequeue activity entries */
        if( xQueueReceive(activity_entry_queue_handle, &activity_entry, portMAX_DELAY) == pdTRUE ){
            /* Create a temporary queue that will be used as a FIFO to store activity entries */
            tmp_queue = xQueueCreate(MAXIMUM_ENTRIES_IN_LOG_FILES, sizeof(line));
            assert(tmp_queue != NULL);

            /* Create new line according to the specified activity type */
            switch(activity_entry.activity_type){
                case ACTIVITY_TYPE_UART:
                {
                    local_remove_carriage_return((char*)activity_entry.u.activity_uart.log);
                    snprintf(line, sizeof(line) - 1,
                                "[%ld] (%ld) - %s\r\n",
                                local_get_timestamp(),
                                activity_entry.u.activity_uart.uart_port,
                                activity_entry.u.activity_uart.log);
                }
                break;

                case ACTIVITY_TYPE_GPIO:
                {
                    snprintf(line, sizeof(line) - 1,
                                "[%ld] %s%s %ld: from %d to %d\r\n",
                                local_get_timestamp(),
                                activity_entry.u.activity_gpio.pin_type ? "A" : "D",
                                activity_entry.u.activity_gpio.direction ? "O" : "I",
                                activity_entry.u.activity_gpio.pin_number,
                                activity_entry.u.activity_gpio.pin_type ? activity_entry.u.activity_gpio.last_value : activity_entry.u.activity_gpio.last_state,
                                activity_entry.u.activity_gpio.pin_type ? activity_entry.u.activity_gpio.current_value : activity_entry.u.activity_gpio.current_state);
                }
                break;

                default:
                    assert(0);
                break;
            }

            /* Add newly created line to the queue */
            assert(xQueueSend(tmp_queue, &line, 0) == pdTRUE);

            /* Check if file already exist */
            struct stat buffer;
            if(stat(a_activity_type_file_name[activity_entry.activity_type], &buffer) == 0){
                /* Open the activity type file in read mode */
                file = fopen(a_activity_type_file_name[activity_entry.activity_type], "rb");
                assert(file != NULL);

                /* 
                *  Copy already stored activity entries from file 
                *  till maximum entries number has been reached 
                */
                uint16_t i = 0;
                while( fgets((char*)&line, sizeof(line), file) != NULL
                    && i < MAXIMUM_ENTRIES_IN_LOG_FILES - 1 ){
                    assert(xQueueSend(tmp_queue, &line, portMAX_DELAY) == pdTRUE);
                    i++;
                }

                fclose(file);
            }

            /* Open the file in write mode */
            file = fopen(a_activity_type_file_name[activity_entry.activity_type], "wb+");

            /* Dequeue the FIFO and add fetched lines to the file */
            while( xQueueReceive(tmp_queue, &line, 0) == pdTRUE )
                fprintf(file, "%s", line);

            /* Close the file */
            fclose(file);

            vQueueDelete(tmp_queue);
            tmp_queue = NULL;
        }
    }
}

/* Function used to remove carriage return if it exists */
static void local_remove_carriage_return( char* buffer ){
    uint16_t log_length = strlen(buffer);
    for(int16_t idx = log_length; idx >= 0; idx--){
        if(buffer[idx] == '\r' || buffer[idx] == '\n')
            buffer[idx] = 0;
    }
    
}

/* Function used to get a timestamp for activity entries */
static uint32_t local_get_timestamp( void ){
    return (uint32_t) (xTaskGetTickCount() * portTICK_PERIOD_MS);
}
