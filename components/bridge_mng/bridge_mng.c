/**
 * @file bridge_mng.c
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge management module
 * @version 0.1
 * @date 2023-05-15
 * 
 * @copyright Copyright (c) 2023
 * 
 * This module is the main component of the project.
 * It acts as a bridge between the embedded system and the exterior world.
 */

#include "bridge_mng.h"
#include "bridge_mng_store.h"
#include "bridge_mng_com.h"
#include "bridge_mng_uart.h"
#include "bridge_mng_gpio.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_MNG";

void bridge_mng_init( void ){
    ESP_LOGI(TAG, "Bridge manager initialization..");

    /* Initialize store submodule */
    bridge_mng_store_init();

    /* Initialize communication server */
    bridge_mng_com_init();

    /* Initialize peripheral components */
    bridge_mng_uart_init();
    bridge_mng_gpio_init();

    ESP_LOGI(TAG, "Bridge manager is up and running.");
}

bool bridge_mng_add_activity_entry( t_activity_entry* activity_entry ){
    bool status = false;

    /* First, store activity into a file */
    bridge_mng_store(activity_entry);

    /* 
     * If activity is coming from the embedded side 
     *      Propagate it through communication server.
     * Else
     *      Propagate it to the embedded side.
     */
    if(activity_entry->activity_origin == ACTIVITY_ORIGIN_EMBEDDED){
        status = bridge_mng_com_new_entry(activity_entry);
    }
    else if(activity_entry->activity_origin == ACTIVITY_ORIGIN_EXTERIOR){
        switch(activity_entry->activity_type){
            case ACTIVITY_TYPE_UART:
            {
                status = bridge_mng_uart_send_activity(activity_entry);
            }
            break;

            case ACTIVITY_TYPE_GPIO:
            {
                status = bridge_mng_gpio_send_activity(activity_entry);
            }
            break;

            default:
                assert(0);
            break;
        }
    }

    return status;
}

/***************************************LOCALS*/
