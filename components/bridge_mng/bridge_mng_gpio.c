/**
 * @file bridge_mng_gpio.c
 * @author Cédric Jardin - MOWA ()
 * @brief Bridge manager GPIO submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 * 
 * This submodule is meant to control activity GPIO pins.
 */

#include "bridge_mng_gpio.h"

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "esp_adc/adc_oneshot.h"
#include "esp_system.h"
#include "esp_timer.h"
#include "sdkconfig.h"

#include "esp_log.h"
static const char TAG[] = "BRIDGE_GPIO";

/**
 * @brief GPIO context structure
 */
typedef struct{
    bool                        configured;         /* Indicate if pin has been configured or not */
    bool                        pin_type;           /*!< GPIO Pin type - 0: digital, 1: analog */
    bool                        direction;          /*!< GPIO Pin direction - 0: input, 1: output */

    bool                        last_state;         /*!< GPIO Pin last state (applicable when digital) */
    bool                        current_state;      /*!< GPIO Pin current state (applicable when digital) */

    adc_oneshot_unit_handle_t*  adc_unit_handle;    /*!< ADC unit handle (GPIO analog input specific) */
    adc_channel_t               adc_channel;        /*!< ADC channel (GPIO analog input specific) */
    uint16_t                    last_value;         /*!< GPIO Pin last value (applicable when analog) */
    uint16_t                    current_value;      /*!< GPIO Pin current value (applicable when analog) */

    uint16_t                    debounce_timer;     /*!< Timer used to debounce GPIO state when configured as input */
}t_bridge_mng_gpio;

#define GPIO_DEBOUNCE_TIME_MS 50

#if defined(CONFIG_IDF_TARGET_ESP32)
// GPIO0 to 39
#define MAX_AVAILABLE_GPIO 40
#elif defined(CONFIG_IDF_TARGET_ESP32S2)
// GPIO0 to 46
#define MAX_AVAILABLE_GPIO 47
#elif defined(CONFIG_IDF_TARGET_ESP32S3)
// GPIO0 to 48
#define MAX_AVAILABLE_GPIO 49
#elif defined(CONFIG_IDF_TARGET_ESP32C2)
// GPIO0 to 20
#define MAX_AVAILABLE_GPIO 21
#elif defined(CONFIG_IDF_TARGET_ESP32C3)
// GPIO0 to 48
#define MAX_AVAILABLE_GPIO 22
#elif defined(CONFIG_IDF_TARGET_ESP32C6)
// GPIO0 to 30
#define MAX_AVAILABLE_GPIO 31
#elif defined(CONFIG_IDF_TARGET_ESP32C6)
// GPIO0 to 30
#define MAX_AVAILABLE_GPIO 31
#elif defined(CONFIG_IDF_TARGET_ESP32H2)
// GPIO0 to 27
#define MAX_AVAILABLE_GPIO 28
#elif
#error "This ESP variant is not supported at the moment."
#endif

/* GPIO contexts */
t_bridge_mng_gpio s_bridge_mng_gpio[MAX_AVAILABLE_GPIO];

/* ADC handles */
adc_oneshot_unit_handle_t adc1_handle;
//Only one ADC unit on esp32c2/c6 and h2
#if !defined(CONFIG_IDF_TARGET_ESP32C2) \
    && !defined(CONFIG_IDF_TARGET_ESP32C6) \
    && !defined(CONFIG_IDF_TARGET_ESP32H2)
adc_oneshot_unit_handle_t adc2_handle;
#endif

/******************LOCALS/UTILS*/
static void bridge_mng_gpio_task( void *arg );
static void bridge_mng_one_ms_timer_callback( void* arg );
static bool local_bridge_mng_gpio_configure_as_digital_input( uint32_t pin_num );
static bool local_bridge_mng_gpio_configure_as_digital_output( uint32_t pin_num );
static bool local_bridge_mng_gpio_configure_as_analog_input( uint32_t pin_num );

void bridge_mng_gpio_init( void ){
    /* Setting up GPIO */
    ESP_LOGI(TAG, "Bridge manager submodule : GPIO..");

    memset(&s_bridge_mng_gpio, 0, sizeof(s_bridge_mng_gpio));

    /* Configure ADCs as oneshot units */
    adc_oneshot_unit_init_cfg_t init_config_adc1 = {
        .unit_id = ADC_UNIT_1,
    };
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config_adc1, &adc1_handle));

#if !defined(CONFIG_IDF_TARGET_ESP32C2) \
    && !defined(CONFIG_IDF_TARGET_ESP32C6) \
    && !defined(CONFIG_IDF_TARGET_ESP32H2)
    adc_oneshot_unit_init_cfg_t init_config_adc2 = {
        .unit_id = ADC_UNIT_2,
    };
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config_adc2, &adc2_handle));
#endif

    /* Setup a periodic timer which will elapse every 1ms (used to manage debounce) */
    const esp_timer_create_args_t one_ms_timer_args = {
            .callback = &bridge_mng_one_ms_timer_callback,
            .name = "bridge_mng_gpio_1ms"
    };
    esp_timer_handle_t one_ms_timer;
    ESP_ERROR_CHECK(esp_timer_create(&one_ms_timer_args, &one_ms_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(one_ms_timer, 1000));

    xTaskCreate(bridge_mng_gpio_task, "bridge_mng_gpio_task", 4096, NULL, 5, NULL);
}

bool bridge_mng_gpio_send_activity( t_activity_entry* activity_entry ){
    bool status = false;
    uint32_t pin_num = activity_entry->u.activity_gpio.pin_number;

    /* GUARD: Make sure that requested pin type corresponds to configured pin type */
    if( s_bridge_mng_gpio[pin_num].pin_type != activity_entry->u.activity_gpio.pin_type ){
        ESP_LOGE(TAG, "Pin is misconfigured, abort..");
        status = false;
    }
    else{
        /* Pin is digital */
        if( s_bridge_mng_gpio[pin_num].pin_type == false ){
            if( s_bridge_mng_gpio[pin_num].direction == true ){
                if( gpio_set_level(pin_num, activity_entry->u.activity_gpio.current_state) == ESP_OK ){
                    s_bridge_mng_gpio[pin_num].current_state = activity_entry->u.activity_gpio.current_state;
                    s_bridge_mng_gpio[pin_num].last_state = activity_entry->u.activity_gpio.current_state;
                    status = true;
                }
                else{
                    ESP_LOGE(TAG, "Failed to set pin %ld to level %d", pin_num, activity_entry->u.activity_gpio.current_state);
                    status = false;
                }
            }
            else{
                ESP_LOGE(TAG, "Pin %ld is not configured as output, abort..", pin_num);
                status = false;
            }
        }
        else{ /* Pin is analog */
            ESP_LOGE(TAG, "Setting an analog output is not supported..");
            status = false;
        } 
    }
    
    return status;
}

bool bridge_mng_gpio_configure_as_digital_input( uint32_t pin_num ){
    if( local_bridge_mng_gpio_configure_as_digital_input(pin_num) == true ){
        /* Reset pin state struct */
        s_bridge_mng_gpio[pin_num].configured = true;
        s_bridge_mng_gpio[pin_num].pin_type = false;
        s_bridge_mng_gpio[pin_num].direction = false;
        s_bridge_mng_gpio[pin_num].last_state = false;
        s_bridge_mng_gpio[pin_num].current_state = false;
        s_bridge_mng_gpio[pin_num].debounce_timer = 0;

        return true;
    }
    else{
        return false;
    }
}

bool bridge_mng_gpio_configure_as_digital_output( uint32_t pin_num ){
    if( local_bridge_mng_gpio_configure_as_digital_output(pin_num) == true ){
        /* Reset pin state struct */
        s_bridge_mng_gpio[pin_num].configured = true;
        s_bridge_mng_gpio[pin_num].pin_type = false;
        s_bridge_mng_gpio[pin_num].direction = true;
        s_bridge_mng_gpio[pin_num].last_state = false;
        s_bridge_mng_gpio[pin_num].current_state = false;
        s_bridge_mng_gpio[pin_num].debounce_timer = 0;

        return true;
    }
    else{
        return false;
    }
}

bool bridge_mng_gpio_configure_as_analog_input( uint32_t pin_num ){
    if( local_bridge_mng_gpio_configure_as_analog_input(pin_num) == true ){
        /* Reset pin state struct */
        s_bridge_mng_gpio[pin_num].configured = true;
        s_bridge_mng_gpio[pin_num].pin_type = true;
        s_bridge_mng_gpio[pin_num].direction = false;
        s_bridge_mng_gpio[pin_num].last_value = 0;
        s_bridge_mng_gpio[pin_num].current_value = 0;
        s_bridge_mng_gpio[pin_num].debounce_timer = 0;

        return true;
    }
    else{
        return false;
    }
}

/***************************************LOCALS*/
static void bridge_mng_gpio_task( void *arg ){

    while (1) {
        /* For every pins that has been configured */
        for( uint8_t idx = 0; idx < MAX_AVAILABLE_GPIO; idx++ ){
            if( s_bridge_mng_gpio[idx].configured ){
                /* Pin is digital */
                if( s_bridge_mng_gpio[idx].pin_type == false ){
                    /* Pin configured as input */
                    if( s_bridge_mng_gpio[idx].direction == false ){
                        bool pin_level = gpio_get_level(idx);

                        /* If pin level differ from saved state */
                        if( pin_level != s_bridge_mng_gpio[idx].current_state ){
                            /* Reload the debounce timer */
                            s_bridge_mng_gpio[idx].debounce_timer = GPIO_DEBOUNCE_TIME_MS;
                        }
                        else{
                            /* Check if current state differ from last state and has been debounced */
                            if( s_bridge_mng_gpio[idx].current_state != s_bridge_mng_gpio[idx].last_state
                                && s_bridge_mng_gpio[idx].debounce_timer == 0 ){
                                /* Send pin activity because state has changed */
                                t_activity_entry s_activity_entry;
                                s_activity_entry.activity_origin = ACTIVITY_ORIGIN_EMBEDDED;
                                s_activity_entry.activity_type = ACTIVITY_TYPE_GPIO;
                                s_activity_entry.u.activity_gpio.pin_number = idx;
                                s_activity_entry.u.activity_gpio.pin_type = s_bridge_mng_gpio[idx].pin_type;
                                s_activity_entry.u.activity_gpio.direction = s_bridge_mng_gpio[idx].direction;
                                s_activity_entry.u.activity_gpio.last_state = s_bridge_mng_gpio[idx].last_state;
                                s_activity_entry.u.activity_gpio.current_state = s_bridge_mng_gpio[idx].current_state;
                                bridge_mng_add_activity_entry(&s_activity_entry);

                                s_bridge_mng_gpio[idx].last_state = s_bridge_mng_gpio[idx].current_state;
                            }
                        }

                        /* Make sure to save current state */
                        s_bridge_mng_gpio[idx].current_state = pin_level;
                    }
                    else{ /* Pin configured as output */
                        /* Just set the level */
                        gpio_set_level(idx, s_bridge_mng_gpio[idx].current_state);
                    }
                }
                else{ /* Pin is analog */
                    /* Pin configured as input */
                    if( s_bridge_mng_gpio[idx].direction == false ){
                        int pin_value = 0;

                        /* Read current pin value */
                        adc_oneshot_read(*s_bridge_mng_gpio[idx].adc_unit_handle,
                                s_bridge_mng_gpio[idx].adc_channel,
                                &pin_value);

                        /* If pin value differ from saved value */
                        if( (uint16_t)pin_value != s_bridge_mng_gpio[idx].current_value ){
                            /* Reload the debounce timer */
                            s_bridge_mng_gpio[idx].debounce_timer = GPIO_DEBOUNCE_TIME_MS;
                        }
                        else{
                            /* Check if current value differ from last value and has been debounced */
                            if( s_bridge_mng_gpio[idx].current_value != s_bridge_mng_gpio[idx].last_value
                                && s_bridge_mng_gpio[idx].debounce_timer == 0 ){
                                /* Send pin activity because value has changed */
                                t_activity_entry s_activity_entry;
                                s_activity_entry.activity_origin = ACTIVITY_ORIGIN_EMBEDDED;
                                s_activity_entry.activity_type = ACTIVITY_TYPE_GPIO;
                                s_activity_entry.u.activity_gpio.pin_number = idx;
                                s_activity_entry.u.activity_gpio.pin_type = s_bridge_mng_gpio[idx].pin_type;
                                s_activity_entry.u.activity_gpio.direction = s_bridge_mng_gpio[idx].direction;
                                s_activity_entry.u.activity_gpio.last_value = s_bridge_mng_gpio[idx].last_value;
                                s_activity_entry.u.activity_gpio.current_value = s_bridge_mng_gpio[idx].current_value;
                                bridge_mng_add_activity_entry(&s_activity_entry);

                                s_bridge_mng_gpio[idx].last_value = s_bridge_mng_gpio[idx].current_value;
                            }
                        }

                        /* Make sure to save current value */
                        s_bridge_mng_gpio[idx].current_value = (uint16_t)pin_value;
                    }
                }
                
            }
        }
        vTaskDelay(1);
    }
}

static void bridge_mng_one_ms_timer_callback( void* arg ){
    for(uint8_t idx = 0; idx < MAX_AVAILABLE_GPIO; idx++){
        if( s_bridge_mng_gpio[idx].configured )
            if( s_bridge_mng_gpio[idx].debounce_timer > 0 ) s_bridge_mng_gpio[idx].debounce_timer--;
    }
}

static bool local_bridge_mng_gpio_configure_as_digital_input( uint32_t pin_num ){
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1 << pin_num;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 1;

    if( gpio_config(&io_conf) == ESP_OK ){
        return true;
    }
    else{
        ESP_LOGE(TAG, "Failed to set pin %ld as digital input", pin_num);
        return false;
    }
}

static bool local_bridge_mng_gpio_configure_as_digital_output( uint32_t pin_num ){
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1 << pin_num;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;

    if( gpio_config(&io_conf) == ESP_OK ){
        return true;
    }
    else{
        ESP_LOGE(TAG, "Failed to set pin %ld as digital output", pin_num);
        return false;
    }
}

static bool local_bridge_mng_gpio_configure_as_analog_input( uint32_t pin_num ){
    bool status = false;
    adc_unit_t adc_unit_id;
    adc_channel_t adc_channel;

    /* Try to find the ADC channel associated to the specified pin_number */
    /* Make sure to save ADC unit handle and channel in pin context */
    if( adc_oneshot_io_to_channel(pin_num, &adc_unit_id, &adc_channel) == ESP_OK ){
        /* Configure found ADC channel with default configuration */
        adc_oneshot_chan_cfg_t config = {
            .bitwidth = ADC_BITWIDTH_DEFAULT,
            .atten = ADC_ATTEN_DB_11,
        };

        if( adc_unit_id == ADC_UNIT_1 ){
            adc_oneshot_config_channel(adc1_handle, adc_channel, &config);
            s_bridge_mng_gpio[pin_num].adc_unit_handle = &adc1_handle;
        }
#if !defined(CONFIG_IDF_TARGET_ESP32C2) \
    && !defined(CONFIG_IDF_TARGET_ESP32C6) \
    && !defined(CONFIG_IDF_TARGET_ESP32H2)
        else if( adc_unit_id == ADC_UNIT_2 ){
            adc_oneshot_config_channel(adc1_handle, adc_channel, &config);
            s_bridge_mng_gpio[pin_num].adc_unit_handle = &adc2_handle;
        }
#endif
        else
            assert(0); // Not managed

        s_bridge_mng_gpio[pin_num].adc_channel = adc_channel;

        status = true;
    }
    else{
        ESP_LOGE(TAG, "Failed to set pin %ld as analog input", pin_num);
        status = false;
    }

    return status;
}
