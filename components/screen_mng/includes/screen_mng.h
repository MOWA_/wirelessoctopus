/**
 * @file screen_mng.h
 * @author Cédric Jardin - MOWA ()
 * @brief Screen management submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 *
 */

#ifndef __SCREEN_MNG_H__
#define __SCREEN_MNG_H__

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Initialize screen manager.
 * 
 * This function handles screen initialization.
 */
void screen_mng_init( bool screen );

#endif //__SCREEN_MNG_H__
