/**
 * @file screen_mng.c
 * @author Cédric Jardin - MOWA ()
 * @brief Screen management submodule
 * @version 0.1
 * @date 2023-05-11
 * 
 * @copyright Copyright (c) 2023
 *
 */
#include "screen_mng.h"
#include "ssd1306.h"

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"
static const char TAG[] = "SCREEN_MNG";

/* Screen definition */
#define SCREEN_MNG_H_PIXELS 128
#define SCREEN_MNG_V_PIXELS 64
#define SCREEN_MNG_LINE_MAX_LENGTH 16

#define SCREEN_MNG_I2C_SDA_PIN 11
#define SCREEN_MNG_I2C_SCL_PIN 12

/* Line buffer */
char line[SCREEN_MNG_LINE_MAX_LENGTH];

/* Screen manager states (FSM) */
typedef enum{
    SCREEN_MNG_STATE_INIT,
    SCREEN_MNG_STATE_DISPLAY_WIFI_INFOS,
    SCREEN_MNG_STATE_DISPLAY_LOG_INFOS,
    SCREEN_MNG_MAX_STATE
}e_screen_mng_state;
e_screen_mng_state screen_mng_state = SCREEN_MNG_STATE_INIT;

/* Screen handle */
SSD1306_t screen;

/***************************************LOCALS*/
static void screen_mng_task( void *arg );

void screen_mng_init( bool screen ){
    /* Setting up screen mng */
    ESP_LOGI(TAG, "Screen manager initialization..");

    if( screen ){
        /* Initialize screen I2C bus and library */
        i2c_master_init(&screen, SCREEN_MNG_I2C_SDA_PIN, SCREEN_MNG_I2C_SCL_PIN, -1);
        ssd1306_init(&screen, 128, 64);

        /* Flush and configure screen befor use */
        ssd1306_clear_screen(&screen, false);
        ssd1306_contrast(&screen, 0xff);

        xTaskCreate(screen_mng_task, "screen_mng_task", 4096, NULL, 8, NULL);
    }

    ESP_LOGI(TAG, "Screen manager is up and running.");
}

/***************************************LOCALS*/
static void screen_mng_task( void *arg ){
    while(1){
        switch(screen_mng_state){
            case SCREEN_MNG_STATE_INIT:
            {
                ssd1306_clear_screen(&screen, false);

                ssd1306_display_text(&screen, 3, " WirelessLogger ", 16, false);
                ssd1306_display_text(&screen, 4, "     V1.0.0     ", 16, false);

                vTaskDelay(3000 / portTICK_PERIOD_MS);

                ssd1306_clear_screen(&screen, false);

                ssd1306_display_text(&screen, 0, " WirelessLogger ", 16, false);
                ssd1306_display_text(&screen, 1, "     V1.0.0     ", 16, false);

                screen_mng_state = SCREEN_MNG_STATE_DISPLAY_WIFI_INFOS;
            }
            break;

            case SCREEN_MNG_STATE_DISPLAY_WIFI_INFOS:
            {
                ssd1306_display_text(&screen, 2, "AP SSID:        ", 16, false);
                ssd1306_display_text(&screen, 3, "WIPUS-323232    ", 16, false);
                ssd1306_display_text(&screen, 4, "----------------", 16, false);
                ssd1306_display_text(&screen, 5, "STA SSID:       ", 16, false);
                ssd1306_display_text(&screen, 6, "CERISE          ", 16, false);

                vTaskDelay(5000 / portTICK_PERIOD_MS);

                screen_mng_state = SCREEN_MNG_STATE_DISPLAY_LOG_INFOS;
            }
            break;

            case SCREEN_MNG_STATE_DISPLAY_LOG_INFOS:
            {
                ssd1306_display_text(&screen, 2, "UART LOGS:      ", 16, false);
                ssd1306_display_text(&screen, 3, "0               ", 16, false);
                ssd1306_display_text(&screen, 4, "----------------", 16, false);
                ssd1306_display_text(&screen, 5, "TOTAL LOGS:     ", 16, false);
                ssd1306_display_text(&screen, 6, "0               ", 16, false);

                vTaskDelay(5000 / portTICK_PERIOD_MS);

                screen_mng_state = SCREEN_MNG_STATE_DISPLAY_WIFI_INFOS;
            }
            break;

            default:
                assert(0); // Should not happen
            break;
        }
    }
}