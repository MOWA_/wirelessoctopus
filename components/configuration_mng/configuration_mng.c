/**
 * @file configuration_mng.c
 * @author Cédric Jardin - MOWA ()
 * @brief Configuration manager
 * @version 0.1
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "configuration_mng.h"

#include "string.h"
#include "nvs_flash.h"

#include "esp_log.h"
static const char TAG[] = "CONFIG_MNG";

/**
 * @brief Config mng context
 */
typedef struct{
    nvs_handle_t                            nvs_handle;                 /*!< NVS storage handle */
}t_configuration_mng_ctx;

static t_configuration_mng_ctx s_configuration_mng_ctx;

void configuration_mng_init( void ){
    esp_err_t err;
    ESP_LOGI(TAG, "Configuration manager initialization.");

    ESP_LOGI(TAG, "Starting NVS flash memory..");

    /* Initialize context */
    memset(&s_configuration_mng_ctx, 0, sizeof(t_configuration_mng_ctx));

    /* NVS initialization */
    err = nvs_flash_init();

    /* Erase NVS storage in case of failure */
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
    ESP_ERROR_CHECK(nvs_open("nvs_storage", NVS_READWRITE, &s_configuration_mng_ctx.nvs_handle));

    ESP_LOGI(TAG, "Configuration manager is up and running.");
}

void configuration_mng_get_sta_ap_ssid_password( char* ap_ssid, char* ap_password ){
    esp_err_t err;
    size_t saved_size;

    /* Try to fetch value and size */
    err = nvs_get_str(s_configuration_mng_ctx.nvs_handle, "ap_ssid", NULL, &saved_size);

    /* If not found, set the default value */
    if( err == ESP_ERR_NVS_NOT_FOUND ){
        strlcpy(ap_ssid, CONFIG_DEFAULT_WIFI_AP, CONFIG_MAX_WIFI_AP_SSID_LENGTH);
        ESP_ERROR_CHECK(nvs_set_str(s_configuration_mng_ctx.nvs_handle, "ap_ssid", CONFIG_DEFAULT_WIFI_AP));
        ESP_ERROR_CHECK(nvs_commit(s_configuration_mng_ctx.nvs_handle));
    }
    else{
        if( saved_size > CONFIG_MAX_WIFI_AP_SSID_LENGTH ) saved_size = CONFIG_MAX_WIFI_AP_SSID_LENGTH;
        ESP_ERROR_CHECK(nvs_get_str(s_configuration_mng_ctx.nvs_handle, "ap_ssid", ap_ssid, &saved_size));
    }

    /* Try to fetch value and size */
    err = nvs_get_str(s_configuration_mng_ctx.nvs_handle, "ap_password", NULL, &saved_size);

    /* If not found, set the default value */
    if( err == ESP_ERR_NVS_NOT_FOUND ){
        strlcpy(ap_password, CONFIG_DEFAULT_WIFI_PASSWORD, CONFIG_MAX_WIFI_PASSWORD_LENGTH);
        ESP_ERROR_CHECK(nvs_set_str(s_configuration_mng_ctx.nvs_handle, "ap_password", CONFIG_DEFAULT_WIFI_PASSWORD));
        ESP_ERROR_CHECK(nvs_commit(s_configuration_mng_ctx.nvs_handle));
    }
    else{
        if( saved_size > CONFIG_MAX_WIFI_PASSWORD_LENGTH ) saved_size = CONFIG_MAX_WIFI_PASSWORD_LENGTH;
        ESP_ERROR_CHECK(nvs_get_str(s_configuration_mng_ctx.nvs_handle, "ap_password", ap_password, &saved_size));
    }
}

void configuration_mng_set_sta_ap_ssid( char* ap_ssid ){
    assert(strlen(ap_ssid) < CONFIG_MAX_WIFI_AP_SSID_LENGTH);

    ESP_ERROR_CHECK(nvs_set_str(s_configuration_mng_ctx.nvs_handle, "ap_ssid", ap_ssid));
    ESP_ERROR_CHECK(nvs_commit(s_configuration_mng_ctx.nvs_handle));
}

void configuration_mng_set_sta_ap_password( char* ap_password ){
    assert(strlen(ap_password) < CONFIG_MAX_WIFI_PASSWORD_LENGTH);

    ESP_ERROR_CHECK(nvs_set_str(s_configuration_mng_ctx.nvs_handle, "ap_password", ap_password));
    ESP_ERROR_CHECK(nvs_commit(s_configuration_mng_ctx.nvs_handle));
}