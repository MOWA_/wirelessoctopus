/**
 * @file configuration_mng.h
 * @author Cédric Jardin - MOWA ()
 * @brief Configuration manager
 * @version 0.1
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __CONFIGURATION_MNG_H__
#define __CONFIGURATION_MNG_H__

#include <stdint.h>

/**************************************FILES/CONFIG*/
#define CONFIG_DEFAULT_WIFI_AP              "DEFAULT_APSSID"
#define CONFIG_DEFAULT_WIFI_PASSWORD        "DEFAULT_APPASS"
#define CONFIG_MAX_WIFI_AP_SSID_LENGTH      32
#define CONFIG_MAX_WIFI_PASSWORD_LENGTH     64

/**
 * @brief Initialize config mng.
 * 
 * Check for mandatory configurations and create them if it doesn't exist.
 */
void configuration_mng_init( void );

/**
 * @brief Fetch configured STA AP SSID and password from storage
 * ap_ssid and ap_password pointers must point to a sufficient allocated space.
 * CONFIG_MAX_WIFI_AP_SSID_LENGTH
 * CONFIG_MAX_WIFI_PASSWORD_LENGTH
 * 
 * @param ap_ssid AP SSID
 * @param ap_password AP Password
 */
void configuration_mng_get_sta_ap_ssid_password( char* ap_ssid, char* ap_password );

void configuration_mng_set_sta_ap_ssid( char* ap_ssid );
void configuration_mng_set_sta_ap_password( char* ap_password );

#endif //__CONFIGURATION_MNG_H__
