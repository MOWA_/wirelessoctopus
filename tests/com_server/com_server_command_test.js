/**
 * WirelessOctopus simple TCP communication server command test
 */

/* Requires */
var net = require('net');

const WIPUS_IP_ADDRESS = "192.168.4.1"
const WIPUS_TCP_SERVER_PORT = 9000
const WIPUS_TEST_COMMAND = "restart"
const WIPUS_TEST_COMMAND_PARAMETER = ""

var client = new net.Socket();
let ack_received = false;

client.connect(WIPUS_TCP_SERVER_PORT, WIPUS_IP_ADDRESS, function () {
	console.log('Connected to WirelessOctopus module');

	client.write(`{"command":"set_gpio_as_digital_input", "parameter":17}`);
});

client.on('data', function (data) {
	console.log('Received: ' + data);
});

client.on('close', function () {
	console.log('Connection closed');
});