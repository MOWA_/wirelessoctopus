/**
 * WirelessOctopus TCP communication server connection stress test
 */

/* Requires */
var net = require('net');

const WILOG_IP_ADRESS = "192.168.4.1"
const WILOG_TCP_SERVER_PORT = 9000

var client = new net.Socket();
let ack_received = false;

client.connect(WILOG_TCP_SERVER_PORT, WILOG_IP_ADRESS, function () {
	console.log('Connected to WirelessOctopus module');

	client.write('{}');
});

client.on('data', function (data) {
	console.log('Received: ' + data);
	client.write('{}');
});

client.on('close', function () {
	console.log('Connection closed');
});