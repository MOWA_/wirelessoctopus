# Getting started

Here is how you can run a Wireless Octopus module:

1. Clone the project
```bash
git clone https://gitlab.com/MOWA_/WirelessOctopus
cd WirelessOctopus
```

2. Configure project build target
```bash
idf.py set-target esp32s3
```

3. Build project and flash it on the board
```bash
idf.py build flash
```

4. You should now see a WiFi access point starting with "WIPUS-". Connect to it and then open a TCP connection to the module IP address.

5. You should now be able to send commands to the module and receive events.

6. Enjoy !