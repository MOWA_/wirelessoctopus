# Communication server API.

By connecting to a WirelessOctopus module via TCP, user can communicate with it through commands and events. These are based on JSON.

## Events

Whenever activity coming from the embedded side is captured by the module, an event is sent to connected TCP clients.

### UART RX event
```json
{
    "log": "some data received by UART"
}
```

### GPIO digital input event
```json
{
    "pin_num": 2,
    "last_state": false,
    "current_state": true
}
```

### GPIO analog input event
```json
{
    "pin_num": 2,
    "last_value": 2076,
    "current_value": 2070
}
```

## Commands
    
### Command : Restart the module
> Module will restart after 5 seconds
```json
{
    "command": "restart"
}
```

### Command : Set WiFi STA SSID
> Module will try to connect to this AP at boot
```json
{
    "command": "set_wifi_sta_ssid",
    "parameter": "WIFI_STA_SSID"
}
```

### Command : Set WiFi STA Password
```json
{
    "command": "set_wifi_sta_password",
    "parameter": "WIFI_STA_PASSWORD"
}
```

### Command : Set UART configuration
```json
{
    "command": "set_baudrate_uart",
    "parameter": {
        "uart_port_num": 1,
        "baudrate": 921600,
        "rx_pin": 23,
        "tx_pin": 24
    }
}
```

### Command : Send data through UART
```json
{
    "command": "send_data_uart",
    "parameter": {
        "uart_port_num": 1,
        "log": "data to send to the embedded side"
    }
}
```

### Command : Set GPIO as a digital output
> Configure a GPIO as digital output
```json
{
    "command": "set_gpio_as_digital_output",
    "parameter": 17 // pin number
}
```

### Command : Set GPIO as a digital input
> Configure a GPIO as digital input
```json
{
    "command": "set_gpio_as_digital_input",
    "parameter": 17 // pin number
}
```

### Command : Set GPIO state (digital output)
> Set level of a GPIO digital output
```json
{
    "command": "set_gpio_state_digital_output",
    "parameter": {
        "pin_num": 17, //pin number
        "state": true // pin level
    }
}
```

### Command : Set GPIO as a analog input
> Configure a GPIO as analog input
```json
{
    "command": "set_gpio_as_analog_input",
    "parameter": 17 // pin number
}
```