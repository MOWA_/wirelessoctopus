# WiFi

To communicate with the exterior world, the Wireless Octopus module exposes multiple endpoints over WiFi.
When working in station mode, will try to connect to an existing WiFi network. When working in Soft AP mode, a device is acting as host of a WiFi network, to which other devices can connect (like your router).
To ensure maximum reliability, the module will act as both (Soft AP + STA). 
- Module is connected to an existing network: user can then access its API by reaching the module IP address.
- Module is not connected to an existing network: user has to connect to module WiFi before trying to access the API.
> NB: Module WiFi is always enabled, even if it is connected to an existing network.

Wireless Octopus AP always start with "WIPUS-" followed by the last 3 bytes of its mac address.